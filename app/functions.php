<?php
/**
 * Here is your custom functions.
 */

/**
 * Public path
 * @param string $path
 * @return string
 */
function ai_invitation_public_path(string $path = ''): string
{
    static $publicPath = '';
    if (!$publicPath) {
        $publicPath = \config('plugin.ai_invitation.app.public_path') ?: run_path('public');
    }
    return path_combine($publicPath, $path);
}

/**
 * Static path
 * @param string $path
 * @return string
 */
function ai_invitation_static_path(string $path = ''): string
{
    static $staticPath = '';
    if (!$staticPath) {
        $staticPath = \config('plugin.ai_invitation.app.static_path') ?: run_path('public');
    }
    return path_combine($staticPath, $path);
}
