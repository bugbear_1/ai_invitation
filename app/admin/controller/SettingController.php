<?php

namespace plugin\ai_invitation\app\admin\controller;
use plugin\admin\app\controller\Crud;
use plugin\ai_invitation\app\service\Setting;
use support\exception\BusinessException;
use support\Request;
use support\Response;
class SettingController extends SettingBase
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        return raw_view('ai-invitation/index');
    }


    /**
     * 获取设置
     *
     * @return Response
     */
    public function select(): Response
    {
        return json(['code' => 0, 'msg' => 'ok', 'data' => [
            'setting' => Setting::getSetting()
        ]]);
    }

    /**
     * 更新设置
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request): Response
    {
        Setting::saveSetting($request->post());
        return $this->json(0);
    }
}