<?php

namespace plugin\ai_invitation\app\admin\controller;

use plugin\admin\app\controller\Base;
use plugin\ai_invitation\app\service\Model;
use plugin\ai_invitation\app\service\Setting;
use support\Request;
use support\Response;

/**
 * 配置设置基类
 */
class SettingBase extends Base
{

    protected $service = Setting::class;


    /**
     * @var string 默认模型
     */
    protected $defaultModel = '';

    /**
     * @var string 默认模型名称
     */
    protected $defaultModelName = '';

}
