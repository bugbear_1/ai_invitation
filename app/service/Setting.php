<?php

namespace plugin\ai_invitation\app\service;

use plugin\admin\app\model\Option;

class Setting extends Base
{
    /**
     * options表对应的name字段
     */
    const OPTION_NAME = 'plugin_ai_invitation.setting';

    /**获取配置
     *
     * @return array|mixed
     */
    public static function getSetting($name = '', $default = null)
    {
        $setting = Option::where('name', static::OPTION_NAME)->value('value');
        $setting = $setting ? json_decode($setting, true) : [];
        return $name ? $setting[$name] : $setting;
    }

    /**
     * 保存配置
     *
     * @param $data
     * @return void
     */
    public static function saveSetting($data)
    {
        if (!$item = Option::where('name', static::OPTION_NAME)->first()) {
            $item = new Option;
        }
        $item->name = static::OPTION_NAME;
        $item->value = json_encode($data, JSON_UNESCAPED_UNICODE);
        $item->save();
    }
}