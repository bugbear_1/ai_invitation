<?php

namespace plugin\ai_invitation\app\service;

use plugin\admin\app\model\Option;

/**
 * AI模型
 */
class Model extends Base
{
    /**
     * options表对应的name字段
     */
    const OPTION_NAME = 'plugin_ai_invitation.models';

    /**获取配置
     *
     * @return array|mixed
     */
    public static function getSetting()
    {
        $items = Option::where('name', static::OPTION_NAME)->value('value');
        return $items ? json_decode($items, true) : [];
    }
}