<?php

namespace plugin\sns\app\model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use plugin\admin\app\model\Base;
use plugin\admin\app\model\User as BaseUser;

/**
 * @property integer $id 主键(主键)
 * @property integer $user_id 用户id
 * @property string $open_id open_id
 * @property string $union_id union_id
 * @property integer $status 禁止登录
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class SnsUser extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sns_users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return BelongsTo
     */
    public function base(): BelongsTo
    {
        return $this->belongsTo(BaseUser::class, 'user_id', 'id');
    }
    
}
