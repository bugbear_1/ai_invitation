<?php

use support\Request;

return [
    'debug' => true,
    'controller_suffix' => 'Controller',
    'controller_reuse' => false,
    'version' => '1.0.0',
    'public_path' => base_path() . DIRECTORY_SEPARATOR . 'plugin' . DIRECTORY_SEPARATOR . 'ai_invitation' . DIRECTORY_SEPARATOR . 'public',
    'static_path' => DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'ai_invitation'
];
