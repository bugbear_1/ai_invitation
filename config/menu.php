<?php

return [
    [
        'title' => 'AI推广',
        'key' => 'plugin_ai_invitation',
        'icon' => 'layui-icon-survey',
        'weight' => 490,
        'type' => 0,
        'children' => [
            [
                'title' => '基础设置',
                'key' => 'plugin\\ai_invitation\\app\\admin\\controller\\SettingController',
                'href' => '/app/ai-invitation/admin/setting',
                'type' => 1,
                'weight' => 800,
            ]
        ]
    ]
];