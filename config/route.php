<?php

use plugin\user\app\controller\LoginController;
use plugin\ai_invitation\app\admin\controller\SettingController;
use Webman\Route;

//Route::any('/app/user/login', [LoginController::class, 'index'])->middleware([LoginControl::class]);
Route::any('/app/ai-invitation/admin/setting', [SettingController::class, 'index']);